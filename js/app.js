/* Gricey.net JS */

/* Dynamic Background */
generateBackground = function() {
	var pattern = Trianglify({
		height: window.innerHeight, //Math.ceil(window.innerHeight * window.devicePixelRatio),
		width: window.innerWidth, //Math.ceil(window.innerWidth * window.devicePixelRatio),
		color_function: function() {
			var rv  = function(base, variance) {
				var v = base + variance * 2 * Math.random() - variance;
				if (v < 0) return 0;
				if (v > 255) return 255;
				return Math.round(v);
			};
			var variance = 5;
			var r = rv(11, variance);
			var g = rv(87, variance);
			var b = rv(147, variance);
			return "rgb("+r+","+g+","+b+")";
		}
	});
	document.body.style.background = "url(" + pattern.png() + ")";
};
window.onresize = function(event) {
	generateBackground();
};
generateBackground();